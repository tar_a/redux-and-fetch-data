let initState = {
    fruits: [
        'fruit 1',
        'fruit 2'
    ]
}

export let fruit = (state = initState, action) => {
    switch (action.type) {
        case 'CREATE_FRUIT': return {
            users: [
                ...state.fruits,
                action.payload
            ]
        }
        default: return state
    }
}
