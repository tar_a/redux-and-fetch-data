import { combineReducers } from "redux"
import {user} from './user'
import {fruit} from './fruit'
export default combineReducers({
    user,
    fruit
})