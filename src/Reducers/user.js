let initState = {
    users: [
        'user 1',
        'user 2'
    ]
}
export let user = (state = initState, action) => {
    switch (action.type) {
        case 'CREATE_USER': return {
            users: [
                ...state.users,
                action.payload
            ]
        }
        case 'DELETE_NEW_USER': {
            let result = state.users.filter((u, i) => {
                return i !== state.users.length-1
            })
            return {
                users: result
            }
        }
        default: return state
    }
}


