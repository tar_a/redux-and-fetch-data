import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import registerServiceWorker from './registerServiceWorker';
import Root from './Root';

// ---- store ----- //
import { Provider } from 'react-redux'
import store from './store'
// ---- store ----- //

ReactDOM.render(
    <Provider store={store}>
        <Root />
    </Provider>,
    document.getElementById('root'));
registerServiceWorker();
