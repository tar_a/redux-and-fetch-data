import React, { Component } from 'react';
import './App.css';
import { connect } from 'react-redux'
import action from './Actions'
class App extends Component {
  handleOnCreateUser = () =>{
    this.props.actionCreateUser("PETER PARKER")
  }
  handleOnDeleteNewUser = () =>{
    this.props.actionDeleteNewUser()
  }
  render() {
    console.log("app props", this.props)
    let { users, fruits } = this.props
    return (
      <div className="App">
        <button onClick={this.handleOnCreateUser}>CREATE USER</button>
        <button onClick={this.handleOnDeleteNewUser}>DELETE NEW USER</button>
        <p className="App-intro">
          {
            users.map((user, index) => {
              return <li key={index}>{user}</li>
            })
          }
        </p>
        <p className="App-intro">
          {
            fruits.map((fruit, index) => {
              return <li key={index}>{fruit}</li>
            })
          }
        </p>
      </div>
    );
  }
}

let mapStateToProps = (stateStore, props) => {
  return {
    users: stateStore.user.users,
    fruits: stateStore.fruit.fruits
  }
}

let mapDispatchToProps = dispatch => {
  return {
    actionCreateUser:(name) => dispatch(action.createUser(name)) ,
    actionDeleteNewUser:() => dispatch(action.deleteNewUser())
  }
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
