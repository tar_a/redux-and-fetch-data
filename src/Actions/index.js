export default {
    createUser:(name) => {
        return {
            type:'CREATE_USER',
            payload:name
        }
    },
    deleteNewUser:() => {
        return {
            type:'DELETE_NEW_USER',
            payload:''
        }
    }
}