import React, { Component } from 'react'
import Axios from 'axios';
import '../App.css'
export default class Fruit extends Component {
    state = {
        fruits: []
    }
    componentDidMount() {
        this.handleOnGetData()
    }
    handleOnGetData = () => {

        //     let get = (url) => {
        //         return new Promise((resolve,reject) => {
        //             setTimeout(() => {
        //                 if(!isErr){
        //                     resolve(data)
        //                 }else{
        //                     reject(err)
        //                 }
        //             },5000)
        //         })
        //     }
        // let res = search('/')
        // console.log(res)
        // get('/').then(data => {})
        // .catch(err => {})


        Axios.get('http://localhost:3000/fruits').then(response => {
            this.setState({
                fruits: response.data
            })
        })
    }

    handleOnCreateData = () => {
        let data = {
            name: "apple",
            weight: "1 kg",
            color: "yellow",
            vitamin: "A"
        }
        Axios.post('http://localhost:3000/fruits', data).then(response => {
            this.handleOnGetData()
        })
    }
    handleOnDeleteData = async  () => {
        let { fruits } = this.state
        if (fruits.length > 0) {
            let current = fruits[fruits.length - 1]
            let idCurrent = current.id
            // Axios.delete('http://localhost:3000/fruits/'+idCurrent).then(response => {
            //     this.handleOnGetData()
            //     console.log("then")
            // })
            // console.log("after")
            try {
                await Axios.delete('http://localhost:3000/fruits/' + idCurrent)
                // this.handleOnGetData()
                 console.log("after")
            } catch (err) {}

        }

    }
    render() {
        return (
            <div className="App">
                <button onClick={this.handleOnCreateData}>CREATE DATA</button>
                <button onClick={this.handleOnDeleteData}>DELETE DATA</button>
                <h1>Fruit</h1>
                {
                    this.state.fruits.map((value, index) => {
                        let { id, name, color } = value
                        return <li key={index}>{`${id} ${name} ${color}`}</li>
                    })
                }
            </div>
        )
    }
}
