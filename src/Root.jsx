import React from 'react'
import { BrowserRouter, Route, Link, Switch } from 'react-router-dom'
import App from './App';
import Fruit from './Components/Fruit';
export default () => {
    return (
        <BrowserRouter>
            <Switch>
                <Route exact path="/" render={() => {
                    return (
                        <div style={{textAlign:'center',fontSize:60}}>
                            <Link  to="/user">user</Link><br/>
                            <Link to="/fruit">fruit</Link>
                        </div>
                    )

                }} />
                <Route path="/user" component={App} />
                <Route path="/fruit" component={Fruit} />
            </Switch>
        </BrowserRouter>
    )
}
